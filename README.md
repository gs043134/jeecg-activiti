# jeecg-activiti

#### 项目介绍
jeecg集成activiti工作流

#### 菜单
流程列表
activitiController.do?processList

我的流程列表
activitiController.do?myProcessList

请假申请
activitiController.do?startPageSelect&startPage=leaveApply.jsp

运行中的流程
activitiController.do?runningProcessList

待领任务
activitiController.do?waitingClaimTask

待办任务
activitiController.do?claimedTask

已办列表
activitiController.do?finishedTask