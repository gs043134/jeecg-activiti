package org.jeecgframework.web.activiti.service;

import org.jeecgframework.core.common.service.CommonService;
import org.jeecgframework.web.activiti.entity.OaLeaveEntity;

public interface ILeaveService extends CommonService {

  void leaveWorkFlowStart(OaLeaveEntity leave);

  OaLeaveEntity getLeave(Long id);

}
